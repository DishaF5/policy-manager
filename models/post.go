package models

import (
	"time"

	"gorm.io/gorm"
)

type Policy struct {
	gorm.Model
	UUID                  string    `gorm:"primaryKey" json:"UUID"`
	PolicyName            string    `json:"PolicyName"`
	Customer              string    `json:"Customer"`
	PolicyPlatformVersion string    `json:"PolicyPlatformVersion"`
	CreatedTime           time.Time `json:"CreatedTime"`
	Updates               []Version `gorm:"foreignKey:PolicyID;references:UUID"`
}

type Version struct {
	gorm.Model
	PolicyID      string    `json:"PolicyID"`
	PolicyXML     string    `json:"PolicyXML"`
	UpdatedTime   time.Time `json:"UpdatedTime"`
	Comment       string    `json:"Comment"`
	PolicyVersion int       `json:"PolicyVersion"`
}
