package services

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"

	//"io/ioutil"
	"log"
	"movies/models"
	"net/http"
	"time"

	"github.com/google/uuid"

	//xsdvalidate "github.com/terminalstatic/go-xsd-validate"
	"gorm.io/gorm"
	//"gorm.io/driver/sqlite"
	//"github.com/goxmldsig/xmlvalidate"
)

var dbconn *gorm.DB

func GetPolicy(w http.ResponseWriter, r *http.Request) {

	id := r.URL.Query().Get("PolicyID")
	policyName := r.URL.Query().Get("PolicyName")

	if id != "" {
		policy, err := GetPolicyByID(id)
		if err != nil {
			http.Error(w, "Failed to retrieve policy by PolicyId due to : "+err.Error(), http.StatusInternalServerError)
			return
		}
		writeResponse(w, policy)
		return
	}

	if policyName != "" {
		policy, err := GetPolicyByPolicyName(policyName)
		if err != nil {
			http.Error(w, "Failed to retrieve policy by PolicyName due to: "+err.Error(), http.StatusInternalServerError)
			return
		}
		writeResponse(w, policy)
		return

	}
	http.Error(w, "ID or PolicyName not provided", http.StatusBadRequest)
}

func GetPolicyByID(id string) (*models.Policy, error) {
	var policy models.Policy

	err := dbconn.Preload("Updates").First(&policy, "uuid = ?", id).Error
	if err != nil {
		return nil, err
	}

	return &policy, nil
}

func GetPolicyByPolicyName(policyName string) (*models.Policy, error) {
	var policy models.Policy
	if err := dbconn.Where("policy_name = ?", policyName).First(&policy).Error; err != nil {
		fmt.Println("Error querying database:", err)
	}

	err := dbconn.Preload("Updates").First(&policy, "uuid = ?", policy.UUID).Error
	if err != nil {
		return nil, err
	}
	return &policy, nil
}

func writeResponse(w http.ResponseWriter, data interface{}) {
	w.Header().Set("Content-Type", "application/json")
	jsonData, err := json.Marshal(data)
	if err != nil {
		http.Error(w, "Failed to serialize data", http.StatusInternalServerError)
		return
	}
	w.Write(jsonData)
}

func CreatePost(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	log.Println("Inside create post")

	err := r.ParseMultipartForm(10 << 20)
	if err != nil {
		http.Error(w, "Failed to parse form data", http.StatusBadRequest)
		return
	}

	var policy models.Policy
	var version models.Version

	policy.PolicyName = r.FormValue("PolicyName")
	policy.Customer = r.FormValue("Customer")
	policy.PolicyPlatformVersion = r.FormValue("PolicyPLatformVersion")
	version.Comment = r.FormValue("Comment")
	currentTime := time.Now()
	newUUID := uuid.New().String()
	policy.UUID = newUUID

	xmlData, _, err := r.FormFile("PolicyXML")
	if err != nil {
		http.Error(w, "XML file not in proper format", http.StatusBadRequest)
		return
	}
	defer xmlData.Close()

	xmlBytes, err := io.ReadAll(xmlData)

	if err != nil {
		http.Error(w, "Failed to read XML content", http.StatusInternalServerError)
		return
	}

	if err = check(&policy, string(xmlBytes)); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	/*
		//XSD Validation------------------
		xsdvalidate.Init()
		defer xsdvalidate.Cleanup()

		// Read file into memory
		data, err := ioutil.ReadFile("example.txt")
		if err != nil {
			fmt.Printf("Error reading file: %v", err)
			return
		}

		// Read XML bytes into memory, assuming you have xmlBytes defined somewhere
		xmlBytes = []byte("<root><element>Test</element></root>")

		xsdhandler, err := xsdvalidate.NewXsdHandlerMem(data, xsdvalidate.ParsErrDefault)
		if err != nil {
			fmt.Println(err)
			panic(err)
		}
		defer xsdhandler.Free()

		err = xsdhandler.ValidateMem(xmlBytes, xsdvalidate.ValidErrDefault)
		if err != nil {
			switch err.(type) {
			case xsdvalidate.ValidationError:
				fmt.Println(err)
				fmt.Printf("Error in line: %d\n", err.(xsdvalidate.ValidationError).Errors[0].Line)
				fmt.Println(err.(xsdvalidate.ValidationError).Errors[0].Message)
			default:
				fmt.Println(err)
			}
		}

	*/
	autoAssgnVersion := 1

	policyRecord := models.Policy{
		Model:                 gorm.Model{},
		UUID:                  newUUID,
		PolicyName:            policy.PolicyName,
		Customer:              policy.Customer,
		PolicyPlatformVersion: policy.PolicyPlatformVersion,
		CreatedTime:           currentTime,
	}
	result := dbconn.Create(&policyRecord)
	if result.Error != nil {
		http.Error(w, "Failed to create policy record: "+result.Error.Error(), http.StatusInternalServerError)
		return
	}

	policyVersion := models.Version{
		Model:         gorm.Model{},
		PolicyVersion: autoAssgnVersion,
		PolicyID:      policyRecord.UUID,
		PolicyXML:     string(xmlBytes),
		UpdatedTime:   currentTime,
		Comment:       version.Comment,
	}

	fmt.Println(&version)

	if err := dbconn.Create(&policyVersion).Error; err != nil {
		http.Error(w, "Failed to create version record: "+err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusCreated)
	fmt.Fprintf(w, "Policy inserted successfully with ID: %s\n", newUUID)
	fmt.Fprintf(w, "comments: %s", version.Comment)
}

func UpdatePolicies(w http.ResponseWriter, r *http.Request) {

	err := r.ParseMultipartForm(20 << 10)
	if err != nil {
		http.Error(w, "Failed to parse form data ", http.StatusBadRequest)
		return
	}
	policyUUID := r.FormValue("UUID")
	updatedComment := r.FormValue("Comment")
	updatedXML := r.FormValue("PolicyXML")

	var latestVersion models.Version

	err = dbconn.Where("policy_id= ?", policyUUID).Order("policy_version desc").First(&latestVersion).Error
	if err != nil {
		fmt.Println(err)
		return
	}

	newVersionNumber := latestVersion.PolicyVersion + 1

	newVersion := models.Version{
		Model:         gorm.Model{},
		PolicyID:      latestVersion.PolicyID,
		PolicyXML:     updatedXML,
		Comment:       updatedComment,
		UpdatedTime:   time.Now(),
		PolicyVersion: newVersionNumber,
	}

	result := dbconn.Create(&newVersion)

	if result.Error != nil {
		http.Error(w, "Failed to insert policy into database: "+result.Error.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "Policy version updated to %v successfully with the id: %s ", newVersionNumber, policyUUID)

}

func DeletePolicies(w http.ResponseWriter, r *http.Request) {
	id := r.URL.Query().Get("PolicyID")
	versionNumber := r.URL.Query().Get("PolicyVersion")

	switch versionNumber {
	case "":
		var policyToDelete models.Policy
		if err := dbconn.Where("uuid = ?", id).First(&policyToDelete).Error; err != nil {
			fmt.Println(err)
			w.WriteHeader(http.StatusNotFound)
			fmt.Fprintf(w, "Policy with ID %s not found", id)
			return
		}

		if err := dbconn.Delete(&policyToDelete).Error; err != nil {
			fmt.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintf(w, "Failed to delete policy with ID %s", id)
			return
		}

		w.WriteHeader(http.StatusOK)
		fmt.Fprintf(w, "Policy with ID %s deleted successfully", id)

	default:
		var versionToDelete models.Version
		if err := dbconn.Where("policy_id = ? AND policy_version = ?", id, versionNumber).First(&versionToDelete).Error; err != nil {
			fmt.Println(err)
			w.WriteHeader(http.StatusNotFound)
			fmt.Fprintf(w, "Policy version %s with policy ID %s not found", versionNumber, id)
			return
		}

		if err := dbconn.Delete(&versionToDelete).Error; err != nil {
			fmt.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintf(w, "Failed to delete policy version %s with policy ID %s", versionNumber, id)
			return
		}

		w.WriteHeader(http.StatusOK)
		fmt.Fprintf(w, "Policy version %s with policy ID %s deleted successfully", versionNumber, id)
	}

}

func check(Policy *models.Policy, xmlBytes string) error {
	if Policy.PolicyName == "" || Policy.Customer == "" || xmlBytes == "" {
		return errors.New("missing fields in request")
	}
	return nil
}

func SetDB(db *gorm.DB) {
	dbconn = db
}
