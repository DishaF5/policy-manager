package router

import (
	"movies/services"

	"github.com/gorilla/mux"
)

func CreateRouter() *mux.Router {
	router := mux.NewRouter()
	router.HandleFunc("/policies", services.GetPolicy).Methods("GET")
	router.HandleFunc("/policies", services.UpdatePolicies).Methods("PUT")
	router.HandleFunc("/policies", services.DeletePolicies).Methods("DELETE")
	router.HandleFunc("/policies", services.CreatePost).Methods("POST")

	return router
}
