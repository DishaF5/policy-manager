package utils

import (
	"fmt"
	"log"
	"movies/models"

	_ "github.com/lib/pq"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

// Substitute your DB details
const (
	host     = "database"
	port     = 5432
	user     = "postgres"
	password = "password"
	dbname   = "policies"
)

func GetConnection() *gorm.DB {

	

	defaultDB, err := gorm.Open(postgres.New(postgres.Config{DSN: fmt.Sprintf("host=%s port=%d database=%s user=%s password=%s sslmode=disable", host, port, dbname, user, password)}), &gorm.Config{})
	if err != nil {
		panic(err)
	}

	
	var count int64
	var db *gorm.DB
	var policy models.Policy
	var version models.Version

	defaultDB.Raw("SELECT COUNT(*) FROM pg_catalog.pg_database WHERE datname = ?", dbname).Scan(&count)
	if count == 0 {
		
		err := defaultDB.Exec(fmt.Sprintf("CREATE DATABASE %s", dbname)).Error
		if err != nil {
			panic(err)
		}
		psqlInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)
		db, err = gorm.Open(postgres.Open(psqlInfo), &gorm.Config{})
		if err != nil {
			panic(err)
		}
		err = db.AutoMigrate(&policy)
		if err != nil {
			panic("failed to perform migration " + err.Error())
		}
		db.Exec("ALTER TABLE policies ADD CONSTRAINT unique_uuid UNIQUE (UUID);")
		err = db.AutoMigrate(&version)
		if err != nil {
			panic("failed to perform migration " + err.Error())
		}
		db.Exec("ALTER TABLE policies ADD CONSTRAINT unique_policyname UNIQUE (policy_name);")

	} else {
		
		psqlInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)
		db, err = gorm.Open(postgres.Open(psqlInfo), &gorm.Config{})
		if err != nil {
			panic(err)
		}
	}

	log.Println("Database connection is active")
	log.Println("DB Connection established...")

	return db
}
