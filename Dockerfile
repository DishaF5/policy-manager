# syntax=docker/dockerfile:1

FROM golang:latest
ENV POSTGRES_USER=postgres
ENV POSTGRES_PASSWORD=password
ENV POSTGRES_DB=waf_policy_db
ENV POSTGRES_PORT=5432
ENV POSTGRES_HOST=http://localhost 
WORKDIR /app
COPY go.mod go.sum ./
RUN go mod download
COPY . .
RUN go build -o app . 
EXPOSE 8001
CMD ["./app"]
