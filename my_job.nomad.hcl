job "myapp" {
  datacenters = ["dc1"]
  type        = "service"

  group "group1" {
    count = 1

    task "database" {
      driver = "docker"

      config {
        image = "postgres"
        ports = ["5433:5432"]
        env   = {
            POSTGRES_USER=postgres
            POSTGRES_PASSWORD=password
            POSTGRES_DB=waf_policy_db
            POSTGRES_PORT=5432
            POSTGRES_HOST=localhost
        }
        volumes = [
          "database:/var/lib/postgresql/data"
        ]

        network {
          mode = "bridge"
          port "databasep" {
            static = 5432
            to =5433
          }
        }
      }

      resources {
        cpu    = 500
        memory = 256
      }

      service {
        name = "database"
        port = "databasep"

        check {
          type     = "tcp"
          interval = "10s"
          timeout  = "2s"
        }
      }
    }

    task "app" {
      driver = "docker"

      config {
        image = "my-go-app"
        ports = ["8001:8001"]
        env   = {
          DATABASE_HOST     = "database"
          DATABASE_PORT     = "5432"
          DATABASE_NAME     = "waf_policy_db"
          DATABASE_USER     = "postgres"
          DATABASE_PASSWORD = "password"
        }
        network {
          mode = "bridge"
          port "app" {
            static = 8001
          }
        }
      }

      resources {
        cpu    = 500
        memory = 256
      }

      service {
        name = "app"
        port = "app"

        check {
          type     = "tcp"
          interval = "10s"
          timeout  = "2s"
        }
      }
    }
  }
}
