package main

import (
	"fmt"
	"log"
	"movies/router"
	"movies/services"
	"movies/utils"
	"net/http"
)

const port = ":8001"

func main() {
	log.Println("In Main App")

	var dbconn = utils.GetConnection()
	services.SetDB(dbconn)
	var appRouter = router.CreateRouter()

	log.Println("Listening on Port", port)
	fmt.Println(http.ListenAndServe(port, appRouter))

	sqlDB, err := dbconn.DB()
	if err != nil {
		panic(err)
	}
	defer sqlDB.Close()
}
